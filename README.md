# OpenGL Pipeline

Initially, this project was a wrapper for OpenGL 3.x+ pipeline, merely for 
easier use. Later, when I have to deal with various tasks, functions are 
added, and it becomes more comprehensive. There might be a lot of bugs, since
 it is designed for individual projects. 
 
 - The code that wraps OpenGL raw functions is located in `src`.
 - The example codes are located at `app`.

 ## Build and Run


 ### Dependencies
 
 - [CMake](https://cmake.org/)
 - [GLFW3](http://www.glfw.org/docs/latest/)
 - [GLEW](http://glew.sourceforge.net/)
 - [GLM](https://glm.g-truc.net/)
 - [OpenCV 2.4.x or 3.x](https://opencv.org/)


 ### Compile
 
 ```bash
 mkdir build
 cd build
 cmake ..
 make -j4
 ```
 
### Run
Go to `./app`, navigate to directories, and run corresponding binaries.
Typically,

- 1~4_project_name are for test usage
- eth_project_name are utils for our decomposition project.

For each app, use `./app_name --help` to view usages.

Dataset `torus` and `bird` are provided as demo datasets. Most binaries in `./app` are runnable without options (except `eth_projection_ply`), and can receive arguments to run other datsets.

### Useful apps
- `eth_gen_normal_texture -m path_to_model -w width -h height` generates normal atlas (in .png and .txt) given the obj with uv coordinates, and target atlas size.

- `eth_projection -c path_to_config -o path_to_output` renders images to the specified output directory provided a config file. A config file includes `model_path` of the **.obj** model, `texture_path`, and information of `calib`. These configs can be found in folder `eth_projection`, but the directories should be changed to make them work.

- `eth_projection_ply -c path_to_config -o path_to_output` renders images from **.ply**. We use a naive (and **NOT ROBUST**) function to load ply files **IN ASCII FORM**, therefore conversions are required before running. Meshlab can do this job. Since ply files in **ASCII FORM** are too large to be held in the repository, `./eth_projection_ply` is by default not runnable.

- `eth_synthesizer -c path_to_config -l path_to_light -o path_to_output` renders images with synthetic objects and lightings. Two kinds of lighting can be configured in lighting config files. If we specify `light_positions`, then it is a spot light. If we specify `light_directions`, then it is a directional light. Spot light and directional light cannot coexist. Currently we have to adjust them manually. The lighting files can be found in `eth_synthesizer`.


