//
// Created by wei on 17-10-16.
//

#ifndef OPENGL_SNIPPET_GLWRAPPER_H
#define OPENGL_SNIPPET_GLWRAPPER_H

#include "../src/core/args.h"
#include "../src/core/camera.h"
#include "../src/core/model.h"
#include "../src/core/program.h"
#include "../src/core/texture.h"
#include "../src/core/uniforms.h"
#include "../src/core/window.h"
#include "../src/core/framebuffer.h"
#include "../src/core/lighting.h"

#endif //OPENGL_SNIPPET_GLWRAPPER_H
