//
// Created by Neo on 16/7/29.
//

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "cxxopts.h"
#include "glwrapper.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Phong shading renderer (without texture)");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "image width", cxxopts::value<int>()->default_value("640"))
      ("h,height", "image height", cxxopts::value<int>()->default_value("480"))
      ("n,name", "window name",
       cxxopts::value<std::string>()->default_value("Phong"))
      ("l,light", "path-to-config recording lighting conditions",
       cxxopts::value<std::string>()->default_value("lighting.yaml"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()->default_value("../../data/bird/meshes/000.obj"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }
  std::string path_to_model = result["model"].as<std::string>();
  std::string path_to_light = result["light"].as<std::string>();
  std::string window_name = result["name"].as<std::string>();
  int width = result["width"].as<int>();
  int height = result["height"].as<int>();

  /// Load models
  gl::Model model;
  model.LoadObj(path_to_model, true);
  gl::Lighting lighting;
  lighting.Load(path_to_light);
  glm::vec3 material_diffuse_color = glm::vec3(1, 1, 0);

  /// Create OpenGL contexts
  gl::Window window(window_name, width, height);
  gl::Camera camera(window.width(), window.height());
  camera.SwitchInteraction(true);

  gl::Program program;
  program.Load("simple_phong_vert.glsl", gl::kVertexShader);
  program.ReplaceMacro("LIGHT_COUNT", lighting.light_count_str, gl::kVertexShader);
  program.Load("simple_phong_frag.glsl", gl::kFragmentShader);
  program.ReplaceMacro("LIGHT_COUNT", lighting.light_count_str, gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "view", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "light", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_power", gl::kFloat);
  uniforms.GetLocation(program.id(), "material_ambient_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "material_specular_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "material_diffuse_color", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_color", gl::kVector3f);

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  std::cout
      << "W/A/S/D: move forward/left/backward/backward\n"
      << "Space/Left-Shift: move upward/downward\n"
      << "I/K/J/L: rotate upward/left/downward/right\n";

  do {
    /// Update viewpoint
    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();//* view * m;
    glm::mat4 view = camera.view();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Specify program
    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);
    uniforms.Bind("view", &view, 1);
    uniforms.Bind("light", lighting.light_positions.data(), lighting.light_count);
    uniforms.Bind("light_power", &lighting.light_power, 1);
    uniforms.Bind("light_color", &lighting.light_color, 1);

    uniforms.Bind("material_ambient_coeff", &lighting.material_ambient_coeff, 1);
    uniforms.Bind("material_specular_coeff", &lighting.material_specular_coeff, 1);
    uniforms.Bind("material_diffuse_color", &material_diffuse_color, 1);

    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
      window.should_close() == 0);

  glfwTerminate();

  return 0;
}