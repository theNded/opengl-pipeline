//
// Created by wei on 18-1-24.
//

#ifndef OPENGL_WRAPPER_LOAD_PROJECTION_3X4_H
#define OPENGL_WRAPPER_LOAD_PROJECTION_3X4_H

#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <opencv2/opencv.hpp>
#include "glwrapper.h"

void LoadCalibFilesInPath(std::string calib_path,
                          int calib_viewpoint_start,
                          int calib_viewpoint_num,
                          std::string calib_filename_format,
                          int calib_width, int calib_height,
                          std::vector<gl::Camera> *cameras) {
  char buff[300];

  for (int viewpoint = calib_viewpoint_start;
       viewpoint < calib_viewpoint_start + calib_viewpoint_num;
       ++viewpoint) {
    snprintf(buff, sizeof(buff), calib_filename_format.c_str(), viewpoint);
    std::string filename = calib_path + "/" + std::string(buff);
    std::ifstream in(filename);

    cv::Mat_<float> P(3, 4);
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 4; ++j) {
        in >> P[i][j];
      }
    }
    std::cout << "calib-" << viewpoint << ": " << std::endl 
              << P << std::endl;

    gl::Camera camera;
    camera.set_mvp_from_hartley(P, calib_width, calib_height);
    cameras->emplace_back(camera);
  }
}

#endif //OPENGL_WRAPPER_LOAD_PROJECTION_3X4_H
