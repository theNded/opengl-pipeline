//
// Created by wei on 18-1-22.
//

#ifndef OPENGL_WRAPPER_WRITE_NORMAL_TEXTURE_H
#define OPENGL_WRAPPER_WRITE_NORMAL_TEXTURE_H

#include <fstream>
#include <string>
#include <sstream>
#include <opencv2/opencv.hpp>

void WriteNormalTexture(std::string path, cv::Mat &normalf,
                        cv::Mat *normalu, cv::Mat *mask) {
  std::stringstream ss;
  std::ofstream out(path);
  if (! out.is_open()) {
    std::cerr << "Invalid path ..." << std::endl;
    return;
  }
  for (int i = 0; i < normalf.rows; ++i) {
    for (int j = 0; j < normalf.cols; ++j) {
      cv::Vec4f n = normalf.at<cv::Vec4f>(i, j);
      if (n[3] != 0) { // alpha channel,
        ss.str("");
        float norm = sqrtf(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
        n[0] /= norm;
        n[1] /= norm;
        n[2] /= norm;
        normalf.at<cv::Vec4f>(i, j) = n;

        ss << i << " " << j << " "
           << n[0] << " " << n[1] << " " << n[2] << std::endl;
        out << ss.str();

        n[0] = 0.5f * n[0] + 0.5f;
        n[1] = 0.5f * n[1] + 0.5f;
        n[2] = 0.5f * n[2] + 0.5f;

        mask->at<unsigned char>(i, j) = 255;
      }
      normalu->at<cv::Vec4b>(i, j)
          = cv::Vec4b(n[0] * 255, n[1] * 255, n[2] * 255, n[3] * 255);
    }
  }

  std::cout << "normal_texture.txt written." << std::endl;
}

#endif //OPENGL_WRAPPER_WRITE_NORMAL_TEXTURE_H
