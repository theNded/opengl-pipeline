//
// Created by wei on 18-1-1.
//

#ifndef OPENGL_WRAPPER_CONFIG_LOADER_H
#define OPENGL_WRAPPER_CONFIG_LOADER_H

#include <string>
#include <opencv2/opencv.hpp>

struct DatasetConfig {
  void Load(std::string config_path) {
    cv::FileStorage fs(config_path, cv::FileStorage::READ);
    if (!fs.isOpened()) {
      std::cerr << "Invalid configuration path !" << std::endl;
    }

    model_path = (std::string) fs["model_path"];
    texture_path = (std::string) fs["texture_path"];
    init_albedo_path = (std::string)fs["init_albedo_path"];
    final_albedo_path = (std::string)fs["final_albedo_path"];

    cv::FileNode fn = fs["calib_viewpoint_start"];
    std::cout << fn.size() << std::endl;
    calib_viewpoint_start = fn.size() > 0 ? (int)fn : 0;

    calib_viewpoint_num = (int) fs["calib_viewpoint_num"];
    calib_path = (std::string) fs["calib_path"];
    calib_filename_format = (std::string) fs["calib_filename_format"];
    calib_image_width = (int) fs["calib_image_width"];
    calib_image_height = (int) fs["calib_image_height"];

    target_image_width = (int)fs["target_image_width"];
    target_image_height = (int)fs["target_image_height"];

    // Deprecated
    intrinsic_type = (int) fs["intrinsic_type"];
    intrinsic_path = (std::string) fs["intrinsic_path"];
    extrinsic_path = (std::string) fs["extrinsic_path"];
  }

  // Model and texture
  std::string model_path;
  std::string texture_path;
  std::string init_albedo_path;
  std::string final_albedo_path;

  // calibration
  int calib_viewpoint_start = 0;
  int calib_viewpoint_num;
  std::string calib_path;
  std::string calib_filename_format;
  int calib_image_width;
  int calib_image_height;

  // target
  int target_image_width;
  int target_image_height;

  // Deprecated
  int intrinsic_type;
  std::string intrinsic_path;
  std::string extrinsic_path;
};

#endif //OPENGL_WRAPPER_CONFIG_LOADER_H
