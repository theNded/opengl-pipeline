// Include standard headers
#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <vector>
#include <string>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

#include "glwrapper.h"
#include "cube.h"

int main() {
  gl::Window window("Cube", 640, 480);
  gl::Camera camera(window.width(), window.height());
  camera.SwitchInteraction(true);

  gl::Program program;
  program.Load("simple_color_vert.glsl", gl::kVertexShader);
  program.Load("simple_color_frag.glsl", gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  8, kPositions);
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  8, kColors);
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_BYTE),
                  36, kIndices);

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_MULTISAMPLE);
  std::cout
      << "W/A/S/D: move forward/left/backward/backward\n"
      << "Space/Left-Shift: move upward/downward\n"
      << "I/K/J/L: rotate upward/left/downward/right\n";

  do {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();

    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);
    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, 0);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
      window.should_close() == 0);

  glfwTerminate();

  return 0;
}

