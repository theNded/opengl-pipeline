function P = DecodeProjectionFromFile(path, w_img, h_img, w_atlas, h_atlas, use_linear)
    fid = fopen(path);
    c = textscan(fid, '%d %d %f %f');
    fclose(fid);
    pu = double(c{1}); 
    pv = double(c{2});
    tu = c{3};
    tv = c{4};
    
    P = DecodeProjection(pu, pv, w_img, h_img, tu, tv, w_atlas, h_atlas, use_linear);
end

