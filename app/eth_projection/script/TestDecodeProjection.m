dataset_path = '../output';

W_atlas = 2048;
H_atlas = 1536;

W_img = 2048;
H_img = 1536;

str = sprintf('%s/texture.png', dataset_path);
atlas = im2double(imread(str));
atlas = ConvertMat2Vec(W_atlas, H_atlas, 3, atlas);

for iter = 1 : 30
fprintf('Loading viewpoint %d\n', iter);

str = sprintf('%s/projection_%d.txt', dataset_path, iter-1);

%% Decode projection matrices in two ways
P_linear = DecodeProjectionFromFile(str, W_img, H_img, W_atlas, H_atlas, true);
P_gaussian = DecodeProjectionFromFile(str, W_img, H_img, W_atlas, H_atlas, false);

str = sprintf('%s/image_%d.png', dataset_path, iter-1);
vec_real = reshape(im2double(imread(str)), W_img*H_img, 3);

%% Projection
vec_gaussian = P_gaussian * atlas;
vec_linear = P_linear * atlas;

%% Compare images
res_gaussian = whos('P_gaussian');
res_linear = whos('P_linear');
byte_to_mb = 1024 * 1024;
fprintf('Gaussian ver. size: %.02fMb, Linear ver. size: %.02fMb, ratio: %f\n', ...
    res_gaussian.bytes / byte_to_mb, ...
    res_linear.bytes / byte_to_mb, ...
    res_gaussian.bytes / res_linear.bytes);

im_gaussian = ConvertVec2Mat(W_img, H_img, 3, vec_gaussian); 
im_linear = ConvertVec2Mat(W_img, H_img, 3, vec_linear); 
im_real = ConvertVec2Mat(W_img, H_img, 3, vec_real);

mse_gaussian = immse(im_gaussian, im_real);
mse_linear = immse(im_linear, im_real);
fprintf('Gaussian ver. mse: %f, Linear ver. mse: %f, ratio: %f\n', ...
    mse_gaussian, mse_linear, mse_gaussian / mse_linear);
end