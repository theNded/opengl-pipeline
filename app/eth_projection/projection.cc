//
// Created by Neo on 21/09/2017.
//

/// Input: mesh(.obj), texture(.png)
///        intrinsic(s) (fx, cx, fy, cy, w, h)
///        extrinsics (4x4 mat)
/// Output: rendered image(.png)
///         pixel-wise texture coordinate map(.txt)
///         => interpreted by MATLAB

#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include <opencv2/opencv.hpp>
#include <iomanip>
#include "glwrapper.h"
#include "cxxopts.h"

#include "../eth_include/write_projection.h"
#include "../eth_include/dataset_config.h"
#include "../eth_include/load_calibs.h"

int main(int argc, char **argv) {
  cxxopts::Options options(
      argv[0], "Generate I(px, py) = (tx, ty) correspondences that "
          "support the building of large sparse MATLAB projection matrices.");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("c,config", "path to configuration (.yaml), specifying details",
       cxxopts::value<std::string>()->default_value("config/demo_bird.yaml"))
      ("o,output", "output path",
       cxxopts::value<std::string>()->default_value("output"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  DatasetConfig config;
  std::string config_path = result["config"].as<std::string>();
  config.Load(config_path);
  std::string output_path = result["output"].as<std::string>();


  /// Load model
  gl::Model model;
  model.LoadObj(config.model_path);

  const int texture_width = config.target_image_width;
  const int texture_height = config.target_image_height;
  // DUMMY
  const int window_width = config.target_image_width;
  const int window_height = config.target_image_height;

  /// Configure OpenGL context
  gl::Window window("Projection", window_width, window_height, false);
  std::vector<gl::Camera> cameras;
  LoadCalibFilesInPath(config.calib_path,
                       config.calib_viewpoint_start,
                       config.calib_viewpoint_num,
                       config.calib_filename_format,
                       config.calib_image_width,
                       config.calib_image_height,
                       &cameras);

  /// Select dataset with different K configurations


  gl::Texture input_texture;
  input_texture.Init(config.texture_path);

  ////////////////////
  /// Shader 0:
  /// - Find accurate per-pixel correspondence, i.e. I(pu, pv) = (tu, tv).
  /// - The EXACT float value is stored in framebuffer without losing precision.
  gl::Program program0;
  program0.Load("shader/pixel_uv_vert.glsl", gl::kVertexShader);
  program0.Load("shader/pixel_uv_frag.glsl", gl::kFragmentShader);
  program0.Build();

  gl::Uniforms uniforms0;
  uniforms0.GetLocation(program0.id(), "mvp", gl::kMatrix4f);

  gl::Args args0(3);
  args0.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.positions().size(), model.positions().data());
  args0.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                   model.uvs().size(), model.uvs().data());
  args0.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                   model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_uv(GL_RGBA32F, texture_width, texture_height);

  ////////////////////
  /// Shader 1:
  /// - Traditional rendering
  /// - Render into framebuffer as a texture, since the image resolution
  ///   might be higher than screen size (e.g. 2560x1920)
  gl::Program program1;
  program1.Load("shader/simple_texture_vert.glsl", gl::kVertexShader);
  program1.Load("shader/simple_texture_frag.glsl", gl::kFragmentShader);
  program1.Build();

  gl::Uniforms uniforms1;
  uniforms1.GetLocation(program1.id(), "mvp", gl::kMatrix4f);
  uniforms1.GetLocation(program1.id(), "tex", gl::kTexture2D);

  gl::Args args1(3);
  args1.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.positions().size(), model.positions().data());
  args1.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                   model.uvs().size(), model.uvs().data());
  args1.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                   model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_image(GL_RGB32F, texture_width, texture_height, true);

  /// Additional settings
  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  glEnable(GL_MULTISAMPLE);

  glEnable(GL_POLYGON_SMOOTH);
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

  glEnable(GL_BLEND);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  glShadeModel(GL_FLAT);

  system(std::string("mkdir -p " + output_path).c_str());
  cv::Mat t = input_texture.image();
  cv::flip(t, t, 0);
  cv::imwrite(output_path + "/texture.png", t);

  /// Iterate viewpoints
  const size_t viewpoint_count = cameras.size();
  for (size_t i = 0; i < viewpoint_count; ++i) {
    std::cout << i << " / " << viewpoint_count << std::endl;

    glm::mat4 mvp = cameras[i].mvp();

    {
      // Shader 0: render I(pu, pv) = (tu, tv) to fbo
      fbo_uv.Bind();
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glUseProgram(program0.id());
      uniforms0.Bind("mvp", &mvp, 1);
      glBindVertexArray(args0.vao());
      glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);

      std::stringstream ss("");
      ss << output_path.c_str() << "/projection_" << i << ".txt";
      // Read from framebuffer by .Capture() and save to file.
      //WriteProjection(ss.str(), fbo_uv.CaptureColor());
    }

    {
      // Shader 1: render image to fbo
      fbo_image.Bind();
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glUseProgram(program1.id());
      input_texture.Bind(0);
      uniforms1.Bind("mvp", &mvp, 1);
      uniforms1.Bind("tex", GLuint(0));
      glBindVertexArray(args1.vao());
      glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);
      fbo_image.Blit();

      std::stringstream ss("");
      ss << output_path.c_str() << "/Image" << i + 1 << ".png";

      cv::Mat imf = fbo_image.CaptureColor(), imu;
      cv::cvtColor(imf, imf, cv::COLOR_RGBA2RGB);
      imf.convertTo(imu, CV_8U);
      cv::imwrite(ss.str(), imu);
    }

//    {
//      cv::Matx33f K = cameras[i].projection_to_hartley(
//          config.target_image_width,
//          config.target_image_height);
//      cv::Matx34f Rt = cameras[i].view_to_hartley();
//      cv::Matx34f P = K * Rt;
//
//      std::stringstream ss("");
//      ss << output_path.c_str() << "/calib_"
//         << std::setw(3) << std::setfill('0') << i << ".txt";
//      std::ofstream calib(ss.str());
//      for (int i = 0; i < 3; ++i) {
//        for (int j = 0; j < 4; ++j) {
//          calib << P(i, j) << " ";
//        }
//        calib << std::endl;
//      }
//    }

    window.swap_buffer();
  }

  glfwTerminate();
  return 0;
}