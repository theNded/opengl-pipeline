#!/bin/sh
mkdir -p origin/front
./eth_synthesizer -c config/toad_fixed_origin.yaml -l lights/directional/lighting_front.yaml -o origin/front
mkdir -p origin/left
./eth_synthesizer -c config/toad_fixed_origin.yaml -l lights/directional/lighting_left.yaml -o origin/left
mkdir -p origin/left+above
./eth_synthesizer -c config/toad_fixed_origin.yaml -l lights/directional/lighting_left+above.yaml -o origin/left+above

mkdir -p smoothed/front
./eth_synthesizer -c config/toad_fixed_smoothed.yaml -l lights/directional/lighting_front.yaml -o smoothed/front
mkdir -p smoothed/left
./eth_synthesizer -c config/toad_fixed_smoothed.yaml -l lights/directional/lighting_left.yaml -o smoothed/left
mkdir -p smoothed/left+above
./eth_synthesizer -c config/toad_fixed_smoothed.yaml -l lights/directional/lighting_left+above.yaml -o smoothed/left+above