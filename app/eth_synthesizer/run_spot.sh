#!/bin/sh
mkdir -p origin/1
./eth_synthesizer -c config/toad_fixed_origin.yaml -l lights/spot/lighting_1.yaml -o origin/1
mkdir -p origin/4
./eth_synthesizer -c config/toad_fixed_origin.yaml -l lights/spot/lighting_4.yaml -o origin/4

mkdir -p smoothed/1
./eth_synthesizer -c config/toad_fixed_smoothed.yaml -l lights/spot/lighting_1.yaml -o smoothed/front
mkdir -p smoothed/4
./eth_synthesizer -c config/toad_fixed_smoothed.yaml -l lights/spot/lighting_4.yaml -o smoothed/left