#version 330 core

#define LIGHT_COUNT 1

// Interpolated values from the vertex shaders
in vec2 uv;

in vec3 position_c;
in vec3 normal_c;
in vec3 light_dir_c[LIGHT_COUNT];

uniform float light_power;
uniform vec3  material_ambient_coeff;
uniform vec3  material_specular_coeff;
uniform vec3  light_color;

// Ouput data
layout(location = 0) out vec3 color;

// Values that stay constant for the whole meshing.
uniform sampler2D texture_sampler;

void main() {
	vec3 diffuse_color = texture(texture_sampler, uv).rgb;

    color = vec3(0, 0, 0);
    vec3 lambertian_factor = vec3(0);
    vec3 specular_factor   = vec3(0);
	for (int i = 0; i < LIGHT_COUNT; ++i) {
        vec3 n = normalize(normal_c);
        vec3 l = normalize(light_dir_c[i]);
        float cos_theta = clamp(dot(n, l), 0, 1);

        vec3 e = -normalize(position_c);
        vec3 r = reflect(-l, n);
        float cos_alpha = clamp(dot(e, r), 0, 1);

        float dist_factor = light_power;
        lambertian_factor += cos_theta * dist_factor;
        specular_factor   += pow(cos_alpha, 32) * dist_factor;
	}

	lambertian_factor = clamp(lambertian_factor, 0, 1);
	specular_factor = clamp(specular_factor, 0, 1);

    color =
        // Ambient : simulates indirect lighting
        diffuse_color * material_ambient_coeff +
        // Diffuse : "color" of the object
        diffuse_color * light_color * lambertian_factor +
        // Specular : reflective highlight, like a mirror
        light_color * material_specular_coeff * specular_factor;
    color = clamp(color, 0, 1);
}