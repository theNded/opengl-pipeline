#version 330 core

layout(location = 0) in vec3 in_position;

uniform mat4 mvp; // K * view
uniform float power;

void main() {
    // clip coordinate
    gl_Position =  mvp * vec4(in_position, 1.0);
    gl_PointSize = 20.0f + power;
}
