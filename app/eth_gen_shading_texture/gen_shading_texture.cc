//
// Created by Neo on 08/08/2017.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <opencv2/opencv.hpp>

#include "glwrapper.h"
#include "cxxopts.h"

int main(int argc, char **argv) {
  cxxopts::Options options(argv[0], "Shading texture generation");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "atlas width", cxxopts::value<int>()->default_value("1024"))
      ("h,height", "atlas height", cxxopts::value<int>()->default_value("768"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()->default_value("../../data/bird/meshes/000.obj"))
      ("l,light", "path-to-light-config (.yaml)",
       cxxopts::value<std::string>()->default_value("lighting.yaml"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model = result["model"].as<std::string>();
  std::string path_to_light = result["light"].as<std::string>();
  int atlas_width = result["width"].as<int>();
  int atlas_height = result["height"].as<int>();

  gl::Model model;
  model.LoadObj(path_to_model);
  gl::Lighting lighting;
  lighting.Load(path_to_light);

  // Context and control init
  gl::Window window("Shading", atlas_width, atlas_height, false);

  gl::Program program;
  program.Load("gen_shading_texture_vert.glsl", gl::kVertexShader);
  program.Load("gen_shading_texture_frag.glsl", gl::kFragmentShader);
  program.ReplaceMacro("LIGHT_COUNT", lighting.light_count_str, gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "light", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_power", gl::kFloat);
  uniforms.GetLocation(program.id(), "light_color", gl::kVector3f);
  uniforms.GetLocation(program.id(), "material_ambient_coeff", gl::kVector3f);

  gl::Args args(4);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                  model.uvs().size(), model.uvs().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(3, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_shaded_texture(GL_RGB, atlas_width, atlas_height);

  /// divided by 2 depends on Resolution: refactor it later
  fbo_shaded_texture.Bind();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(program.id());
  uniforms.Bind("light", lighting.light_positions.data(), lighting.light_count);
  uniforms.Bind("light_power", &lighting.light_power, 1);
  uniforms.Bind("light_color", &lighting.light_color, 1);
  uniforms.Bind("material_ambient_coeff", &lighting.material_ambient_coeff, 1);
  glBindVertexArray(args.vao());
  glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  cv::imwrite("shading_texture.png", fbo_shaded_texture.CaptureColor());
  std::cout << "shading_texture.png written." << std::endl;

  return 0;
}