#version 330 core

#define LIGHT_COUNT 1

in vec2 uv;
in vec3 position;
in vec3 normal;

layout(location = 0) out vec3 shading;

uniform vec3  light[LIGHT_COUNT];
uniform vec3  material_ambient_coeff;
uniform float light_power;
uniform vec3  light_color;

void main() {
    vec3 lambertian_factor = vec3(0);
    for (int i = 0; i < LIGHT_COUNT; ++i) {
        float distance = length(light[i] - position);

        vec3 n = normalize(normal);
        vec3 l = normalize(light[i] - position);
        float cos_theta = clamp(dot(n, l), 0, 1);

        float dist_factor = light_power / (distance * distance);
        lambertian_factor += cos_theta * dist_factor;
    }
    lambertian_factor = clamp(lambertian_factor, 0, 1);

    shading =
        // Ambient : simulates indirect lighting
        material_ambient_coeff +
        // Diffuse : "color" of the object
        light_color * lambertian_factor;
    shading = clamp(shading, 0, 1);
}