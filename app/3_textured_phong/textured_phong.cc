//
// Created by Neo on 16/7/29.
//

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "glwrapper.h"
#include "cxxopts.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Phong shading renderer (with texture)");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "image width", cxxopts::value<int>()->default_value("640"))
      ("h,height", "image height", cxxopts::value<int>()->default_value("480"))
      ("n,name", "window name",
       cxxopts::value<std::string>()->default_value("Phong Textured"))
      ("l,light", "path-to-config recording lighting conditions",
       cxxopts::value<std::string>()->default_value("lighting.yaml"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()->default_value("../../data/bird/meshes/000.obj"))
      ("t,texture", "path-to-texture (.png)",
       cxxopts::value<std::string>()->default_value("../../data/bird/textures/000.png"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model = result["model"].as<std::string>();
  std::string path_to_texture = result["texture"].as<std::string>();
  std::string path_to_light = result["light"].as<std::string>();
  std::string window_name = result["name"].as<std::string>();
  int width = result["width"].as<int>();
  int height = result["height"].as<int>();

  gl::Model model;
  model.LoadObj(path_to_model);
  gl::Lighting lights;
  lights.Load(path_to_light);

  // Context and control init
  gl::Window window(window_name, width, height);
  gl::Camera camera(window.width(), window.height());
  camera.SwitchInteraction(true);

  gl::Texture texture;
  texture.Init(path_to_texture);

  gl::Program program;
  program.Load("textured_phong_vert.glsl", gl::kVertexShader);
  program.ReplaceMacro("LIGHT_COUNT", lights.light_count_str,
                       gl::kVertexShader);
  program.Load("textured_phong_frag.glsl", gl::kFragmentShader);
  program.ReplaceMacro("LIGHT_COUNT", lights.light_count_str,
                       gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "view", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "texture_sampler", gl::kTexture2D);
  uniforms.GetLocation(program.id(), "light", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_power", gl::kFloat);
  uniforms.GetLocation(program.id(), "material_ambient_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "material_specular_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_color", gl::kVector3f);

  gl::Args args(4);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                  model.uvs().size(), model.uvs().data());
  args.BindBuffer(3, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  std::cout
      << "W/A/S/D: move forward/left/backward/backward\n"
      << "Space/Left-Shift: move upward/downward\n"
      << "I/K/J/L: rotate upward/left/downward/right\n";
  do {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Update viewpoint
    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();
    glm::mat4 view = camera.view();

    glUseProgram(program.id());
    texture.Bind(0);
    uniforms.Bind("mvp", &mvp, 1);
    uniforms.Bind("view", &view, 1);
    uniforms.Bind("texture_sampler", GLuint(0));
    uniforms.Bind("light", lights.light_positions.data(), lights.light_count);
    uniforms.Bind("light_power", &lights.light_power, 1);
    uniforms.Bind("light_color", &lights.light_color, 1);
    uniforms.Bind("material_ambient_coeff", &lights.material_ambient_coeff, 1);
    uniforms.Bind("material_specular_coeff", &lights.material_specular_coeff, 1);

    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
      window.should_close() == 0);

  glfwTerminate();

  return 0;
}