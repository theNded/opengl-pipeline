//
// Created by Neo on 08/08/2017.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <opencv2/opencv.hpp>
#include "glwrapper.h"
#include "cxxopts.h"
#include "../eth_include/write_normal_texture.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Normal texture generation");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "atlas width", cxxopts::value<int>()->default_value("1024"))
      ("h,height", "atlas height", cxxopts::value<int>()->default_value("768"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()->default_value("../../data/bird/meshes/000.obj"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model = result["model"].as<std::string>();
  int atlas_width = result["width"].as<int>();
  int atlas_height = result["height"].as<int>();

  gl::Model model;
  model.LoadObj(path_to_model);

  // No need to show the window
  gl::Window window("Normal", atlas_width / 2, atlas_height / 2, false);
  gl::Program program;
  program.Load("gen_normal_texture_vert.glsl", gl::kVertexShader);
  program.Load("gen_normal_texture_frag.glsl", gl::kFragmentShader);
  program.Build();

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                  model.uvs().size(), model.uvs().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_normal(GL_RGBA32F, atlas_width, atlas_height);

  // render
  fbo_normal.Bind();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(program.id());
  glBindVertexArray(args.vao());
  glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  cv::Mat normalf = fbo_normal.CaptureColor();

  cv::Mat normalu = cv::Mat(normalf.rows, normalf.cols, CV_8UC4);
  cv::Mat mask = cv::Mat::zeros(normalf.rows, normalf.cols, CV_8U);

  WriteNormalTexture("normal_atlas.txt", normalf, &normalu, &mask);

  cv::cvtColor(normalu, normalu, CV_BGRA2RGBA);
  cv::imwrite("normal_img.png", normalu);
  cv::imwrite("mask_texture.png", mask);
  std::cout << "[normal_texture | mask_texture].png written." << std::endl;
  return 0;
}