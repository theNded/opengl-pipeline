./eth_multi_textures_calibs -c config/beethoven.yaml -l lights/lights2_specular0.yaml -o Beethoven/lights2_specular0
./eth_multi_textures_calibs -c config/beethoven.yaml -l lights/lights2_specular1.yaml -o Beethoven/lights2_specular1
./eth_multi_textures_calibs -c config/beethoven.yaml -l lights/lights5_specular0.yaml -o Beethoven/lights5_specular0
./eth_multi_textures_calibs -c config/beethoven.yaml -l lights/lights5_specular1.yaml -o Beethoven/lights5_specular1

./eth_multi_textures_calibs -c config/bunny.yaml -l lights/lights2_specular0.yaml -o Bunny/lights2_specular0
./eth_multi_textures_calibs -c config/bunny.yaml -l lights/lights2_specular1.yaml -o Bunny/lights2_specular1
./eth_multi_textures_calibs -c config/bunny.yaml -l lights/lights5_specular0.yaml -o Bunny/lights5_specular0
./eth_multi_textures_calibs -c config/bunny.yaml -l lights/lights5_specular1.yaml -o Bunny/lights5_specular1

./eth_multi_textures_calibs -c config/bird.yaml -l lights/bird_lights2_specular0.yaml -o Bird/lights2_specular0
./eth_multi_textures_calibs -c config/bird.yaml -l lights/bird_lights2_specular1.yaml -o Bird/lights2_specular1
./eth_multi_textures_calibs -c config/bird.yaml -l lights/bird_lights5_specular0.yaml -o Bird/lights5_specular0
./eth_multi_textures_calibs -c config/bird.yaml -l lights/bird_lights5_specular1.yaml -o Bird/lights5_specular1
