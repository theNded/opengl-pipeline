//
// Created by wei on 18-1-24.
//

#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

int main() {
  std::ifstream in("calib_torus.txt");
  cv::Mat_<float> m(3, 4);
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 4; ++j) {
      in >> m[i][j];
    }
  }

  std::cout << m << std::endl;
  cv::Mat_<float> K, R, t;
  cv::decomposeProjectionMatrix(m, K, R, t);
  t = t / t[3][0];
  cv::Mat_<float> t_normalized = t.rowRange(0, 3);

  std::cout << K << std::endl;
  std::cout << R << std::endl;
  std::cout << K*R << std::endl;
  std::cout << -R*t_normalized << std::endl;
}
