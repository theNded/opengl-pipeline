//
// Created by wei on 18-1-19.
//

// Include standard headers
#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "cube.h"
#include "glwrapper.h"
#include "rect.h"

int main() {
  gl::Window window("Cube", 640, 480);
  gl::Camera camera(window.visual_width(), window.visual_height());
  camera.SwitchInteraction(true);

  gl::Program program;
  program.Load("textured_phong_vert.glsl", gl::kVertexShader);
  program.Load("textured_phong_frag.glsl", gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "render_color", gl::kInt);

  gl::Model model;
  model.LoadObj("../../model/beethoven/model.obj");
//  std::vector<int> indices;
//  for (int i = 0; i < model.positions().size(); ++i) {
//    indices.emplace_back(i);
//  }

  std::vector<int> indices;
  for (int i = 0; i < 8; ++i) {
    indices.emplace_back(i+1);
  }

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  8, kPositions);
                  //model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  indices.size(), indices.data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_BYTE),
                  36, kIndices);
                  //model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo(GL_RGBA32F, window.visual_width(), window.visual_height
      ());
//
  gl::Program program_pass2;
  program_pass2.Load("image_vert.glsl", gl::kVertexShader);
  program_pass2.Load("image_frag.glsl", gl::kFragmentShader);
  program_pass2.Build();

  gl::Uniforms uniforms_pass2;
  uniforms_pass2.GetLocation(program_pass2.id(), "tex", gl::kTexture2D);

  gl::Args args_pass2(1);
  args_pass2.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3), 6,
                        (void *)kVertices);

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LESS);

  do {
    camera.UpdateView(window);
    fbo.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 mvp = camera.mvp();
    int render_color;

    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);

    // NOW WE CAN GET VISIBILITY
    glBindVertexArray(args.vao());
    render_color = 1;
    uniforms.Bind("render_color", &render_color, 1);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, 0);
    //glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);

//    render_color = 1;
//    uniforms.Bind("render_color", &render_color, 1);
//    glDrawArrays(GL_POINTS, 0, model.positions().size());
//    glBindVertexArray(0);

    std::unordered_map<int, int> is;
    cv::Mat a = fbo.Capture();
    for (int r = 0; r < a.rows; ++r) {
      for (int c = 0; c < a.cols; ++c) {
        cv::Vec4f color = a.at<cv::Vec4f>(r, c);
        if (color[0] != 0) {
          if (is.find(color[0]) != is.end()) {
            is[color[0]] ++;
          } else {
            is[color[0]] = 1;
          }
        }
      }
    }
    for (auto &&iter : is) {
      std::cout << iter.first << " " << iter.second << std::endl;
    }
    std::cout << "--------" << std::endl;

    window.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program_pass2.id());
    fbo.texture().Bind(1);
    uniforms_pass2.Bind("tex", GLuint(1));
    glBindVertexArray(args_pass2.vao());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           window.should_close() == 0);

  glfwTerminate();

  return 0;
}
