//
// Created by Neo on 16/7/29.
//

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <map>
#include <unordered_map>
#include <bits/unordered_map.h>

#include "cxxopts.h"
#include "glwrapper.h"
#include "../eth_include/load_calibs.h"
#include "../eth_include/dataset_config.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Phong shading renderer (with texture)");
  options.positional_help("").show_positional_help();
  options.add_options()("help", "show usages")
      ("n,name", "window name",
       cxxopts::value<std::string>()->default_value("Phong Textured"))
      ("l,light", "path-to-config recording lighting conditions",
       cxxopts::value<std::string>()->default_value("lights.yaml"))
      ("c,config", "path-to-config (.yaml)",
       cxxopts::value<std::string>()->default_value("beethoven.yaml"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_light, path_to_config, window_name;
  if (result.count("light")) {
    path_to_light = result["light"].as<std::string>();
  }
  if (result.count("config")) {
    path_to_config = result["config"].as<std::string>();
  }
  if (result.count("name")) {
    window_name = result["name"].as<std::string>();
  }

  // Context and control init
  DatasetConfig config;
  config.Load(path_to_config);
  gl::Lighting lights;
  lights.Load(path_to_light);

  gl::Model model;
  model.LoadObj(config.model_path);

  const int texture_width = config.target_image_width;
  const int texture_height = config.target_image_height;
  // DUMMY
  const int window_width = config.target_image_width;
  const int window_height = config.target_image_height;
  gl::Window window(window_name, window_width, window_height);
  std::vector<gl::Camera> cameras;
  LoadCalibFilesInPath(config.calib_path,
                       config.calib_viewpoint_start,
                       config.calib_viewpoint_num,
                       config.calib_filename_format,
                       config.calib_image_width,
                       config.calib_image_height,
                       &cameras);

  gl::Texture texture;
  texture.Init(config.texture_path);

  gl::Program program;
  program.Load("img_vtx_color_vert.glsl", gl::kVertexShader);
  program.Load("img_vtx_color_frag.glsl", gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "render_color", gl::kInt);

  std::vector<float> indices;
  for (size_t i = 0; i < model.positions().size(); ++i) {
    indices.emplace_back(float(i));
  }

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT), indices.size(),
                  indices.data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo(GL_RGBA32F, texture_width, texture_height, false);

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  cv::Mat m;
  cv::Mat im;
  for (int iter = 0; iter < 1; ++iter) {
    fbo.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Update viewpoint
    glm::mat4 mvp = cameras[iter].mvp();
    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);
    int render_color = 1;
    uniforms.Bind("render_color", &render_color, 1);
    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    fbo.Blit();

    std::map<int, int> visible_faces;
    m = fbo.CaptureColor();
    im = cv::Mat(m.rows, m.cols, CV_8UC1);
    for (int i = 0; i < m.rows; ++i) {
      for (int j = 0; j < m.cols; ++j) {
        // c[0] -- begin with 0
        cv::Vec4f c = m.at<cv::Vec4f>(i, j);
        if (c[0] != 0) {
          std::cout << c[0] << std::endl;
          if (visible_faces.find(c[0]) != visible_faces.end()) {
            visible_faces[c[0]]++;
          } else {
            visible_faces.emplace(c[0], 1);
          }
          im.at<unsigned char>(i, j) = 255;
        } else {
          im.at<unsigned char>(i, j) = 0;
        }
      }
    }

    gl::Model model_i;
    model_i.positions().clear();
    model_i.uvs().clear();
    model_i.normals().clear();
    model_i.indices().clear();

    std::unordered_map<int, int> vertex_idx_remap;
    int vertex_new_idx = 0;
    for (auto &&i : visible_faces) {
      // face_idx (begin from 0)
      int face_idx = i.first - 1;

      unsigned int f[3] = {
          // real vertex_idx (in indices)
          model.indices()[3*face_idx + 0],
          model.indices()[3*face_idx + 1],
          model.indices()[3*face_idx + 2]
      };

      for (int j = 0; j < 3; ++j) {
        // is this vertex allocated ?
        if (vertex_idx_remap.find(f[j]) == vertex_idx_remap.end()) {
          vertex_idx_remap.emplace(f[j], vertex_new_idx);
          vertex_new_idx ++;
          glm::vec4 uv_homo = mvp*glm::vec4(model.positions()[f[j]], 1);
          glm::vec2 uv = glm::vec2(uv_homo[0]/uv_homo[2],
                                   uv_homo[1]/uv_homo[2]);
          uv = 0.5f * (uv + glm::vec2(1.0f));

          model_i.positions().emplace_back(glm::vec3(model.uvs()[f[j]], 0.0));
          model_i.normals().emplace_back(model.normals()[f[j]]);
          model_i.uvs().emplace_back(uv);
        }
        model_i.indices().emplace_back(vertex_idx_remap[f[j]] + 1);
      }
    }
    cv::imwrite("test.png", im);

    model_i.SaveObj("test.obj");

    window.swap_buffer();
  }

  glfwTerminate();

  return 0;
}