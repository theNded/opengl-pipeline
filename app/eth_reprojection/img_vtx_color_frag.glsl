#version 330 core

layout(location = 0) out vec4 final_color;

in vec3 color;
uniform int render_color;

void main() {
  final_color = (render_color != 0) ? vec4(gl_PrimitiveID, 0, 0, 0) : vec4(0);
}