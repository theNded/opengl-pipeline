./eth_projection_ply -c config/fountain_zollhoefer_original.yaml -o fountain_zollhoefer_original_1280x1024
./eth_projection_ply -c config/fountain_zollhoefer_original2x.yaml -o fountain_zollhoefer_original_2560x2048
./eth_projection_ply -c config/fountain_zollhoefer_refined.yaml -o fountain_zollhoefer_refined_1280x1024
./eth_projection_ply -c config/fountain_zollhoefer_refined2x.yaml -o fountain_zollhoefer_refined_2560x2048
./eth_projection_ply -c config/fountain_ours_svsh.yaml -o fountain_ours_svsh_1280x1024
./eth_projection_ply -c config/fountain_ours_svsh2x.yaml -o fountain_ours_svsh_2560x2048
