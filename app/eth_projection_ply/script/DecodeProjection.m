function P = DecodeProjection(pu, pv, w_img, h_img, tu, tv, w_atlas, h_atlas, use_linear)

%% MATLAB stretch a matrix row-wise
pu = pu + 1;
pv = pv + 1;

%%% OPENGL TEXEL OFFSET -0.5, DAMN IT
tv = (1-tv)*h_atlas + 1 - 0.5;
tu = tu*w_atlas + 1 - 0.5;

%% Gaussian interpolation - kernel size = 3
if (~use_linear)
v1 = max(min(round(tv), h_atlas), 1);
v0 = max(v1 - 1, 1);
v2 = min(v1 + 1, h_atlas);
u1 = max(min(round(tu), w_atlas), 1);
u0 = max(u1 - 1, 1);
u2 = min(u1 + 1, w_atlas);

g = @(x,y,x0,y0) (exp(-( (x-x0).*(x-x0) + (y-y0).*(y-y0) )/2 ));
g0 = g(tu,tv,u0,v0); g1 = g(tu,tv,u0,v1); g2 = g(tu,tv,u0,v2);
g3 = g(tu,tv,u1,v0); g4 = g(tu,tv,u1,v1); g5 = g(tu,tv,u1,v2);
g6 = g(tu,tv,u2,v0); g7 = g(tu,tv,u2,v1); g8 = g(tu,tv,u2,v2);
s = g0+g1+g2+g3+g4+g5+g6+g7+g8;

Si = repmat((pv-1)*h_img+pu, 9, 1);
Sj = [(u0-1)*h_atlas+v0; (u0-1)*h_atlas+v1; (u0-1)*h_atlas+v2; ...
      (u1-1)*h_atlas+v0; (u1-1)*h_atlas+v1; (u1-1)*h_atlas+v2; ...
      (u2-1)*h_atlas+v0; (u2-1)*h_atlas+v1; (u2-1)*h_atlas+v2];

Sv = [g0; g1; g2; g3; g4; g5; g6; g7; g8];
Sv = Sv ./repmat(s, 9, 1);

P = sparse(Si, Sj, Sv, w_img*h_img, w_atlas*h_atlas);

else 

%% Bilinear-interpolation - according to the OpenGL specifications
u0 = min(max(floor(tu), 1), w_atlas);
v0 = min(max(floor(tv), 1), h_atlas);
u1 = min(max(u0 + 1, 1), w_atlas);
v1 = min(max(v0 + 1, 1), h_atlas);
a = tu - u0;
b = tv - v0;

Si = repmat((pv-1)*h_img+pu, 4, 1);

Sj = [(u0-1)*h_atlas + v0; (u1-1)*h_atlas + v0; ...
      (u0-1)*h_atlas + v1; (u1-1)*h_atlas + v1];

Sv = [(1-a).*(1-b); a.*(1-b); (1-a).*b; a.*b];

P = sparse(Si, Sj, Sv, h_img*w_img, h_atlas*w_atlas);
end

end
