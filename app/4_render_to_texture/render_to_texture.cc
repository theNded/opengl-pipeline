//
// Created by Neo on 24/08/2017.
//

#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "glwrapper.h"
#include "rect.h"
#include <opencv2/opencv.hpp>
#include "cxxopts.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Phong shading renderer (with texture)");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "image width", cxxopts::value<int>()->default_value("640"))
      ("h,height", "image height", cxxopts::value<int>()->default_value("480"))
      ("n,name", "window name",
       cxxopts::value<std::string>()->default_value("Phong Textured"))
      ("l,light", "path-to-config recording lighting conditions",
       cxxopts::value<std::string>()->default_value("lighting.yaml"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()->default_value("../../data/bird/meshes/000.obj"))
      ("t,texture", "path-to-texture (.png)",
       cxxopts::value<std::string>()
           ->default_value("../../data/bird/textures/000.png"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model = result["model"].as<std::string>();
  std::string path_to_texture = result["texture"].as<std::string>();
  std::string path_to_light = result["light"].as<std::string>();
  std::string window_name = result["name"].as<std::string>();
  int width = result["width"].as<int>();
  int height = result["height"].as<int>();

  gl::Model model;
  model.LoadObj(path_to_model);

  gl::Window window(window_name, width, height);
  gl::Texture input_texture;
  input_texture.Init(path_to_texture);

  gl::Camera camera(window.width(), window.height());
  camera.SwitchInteraction(true);

  // Pass 1: render to texture
  gl::Program program_pass1;
  program_pass1.Load("simple_texture_vert.glsl", gl::kVertexShader);
  program_pass1.Load("simple_texture_frag.glsl", gl::kFragmentShader);
  program_pass1.Build();

  gl::Uniforms uniforms_pass1;
  uniforms_pass1.GetLocation(program_pass1.id(), "mvp", gl::kMatrix4f);
  uniforms_pass1.GetLocation(program_pass1.id(), "tex", gl::kTexture2D);

  gl::Args args_pass1(3);
  args_pass1.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                        model.positions().size(), model.positions().data());
  args_pass1.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                        model.uvs().size(), model.uvs().data());
  args_pass1.BindBuffer(2,
                        gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                        model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_pass1(GL_RGBA, width*2, height*2, true);

  // Pass 2: render image (we can also test this using OpenCV imshow)
  gl::Program program_pass2;
  program_pass2.Load("image_vert.glsl", gl::kVertexShader);
  program_pass2.Load("image_frag.glsl", gl::kFragmentShader);
  program_pass2.Build();

  gl::Uniforms uniforms_pass2;
  uniforms_pass2.GetLocation(program_pass2.id(), "tex", gl::kTexture2D);

  gl::Args args_pass2(2);
  args_pass2.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3), 6,
                        (void *) kVertices);
  args_pass2.BindBuffer(1,
                        gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                        model.indices().size(), model.indices().data());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  std::cout
      << "W/A/S/D: move forward/left/backward/backward\n"
      << "Space/Left-Shift: move upward/downward\n"
      << "I/K/J/L: rotate upward/left/downward/right\n"
      << "ENTER: take screenshot\n";

  do {
    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();

    // Pass 1:
    // Render to framebuffer, into texture
    fbo_pass1.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program_pass1.id());
    input_texture.Bind(0);
    uniforms_pass1.Bind("mvp", &mvp, 1);
    uniforms_pass1.Bind("tex", GLuint(0));
    glBindVertexArray(args_pass1.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    fbo_pass1.Blit();
    if (window.get_key(GLFW_KEY_ENTER) == GLFW_PRESS) {
      cv::imwrite("texture.png", fbo_pass1.CaptureColor());
      std::cout << "Screenshot taken." << std::endl;
    }

    // Pass 2:
    // Test rendering texture
    window.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program_pass2.id());
    fbo_pass1.color_texture().Bind(1);
    uniforms_pass2.Bind("tex", GLuint(1));
    glBindVertexArray(args_pass2.vao());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
      window.should_close() == 0);

  glfwTerminate();
  return 0;
}