//
// Created by Neo on 22/08/2017.
//

#ifndef OPENGL_SNIPPET_MODEL_H
#define OPENGL_SNIPPET_MODEL_H

#include <glm/glm.hpp>
#include <string>
#include <vector>

namespace gl {
class Model {
 public:
  /// Model m = Model(); m.LoadObj(obj_path);
  /// or Model m = Model(obj_path);
  Model() = default;

  explicit Model(std::string path);

  void LoadObj(std::string path, bool with_uv = true);
  void SaveObj(std::string path, bool with_uv = true);

  void LoadPly(std::string path);

  std::vector<glm::vec3> &positions() { return positions_; }
  std::vector<glm::vec3> &colors() { return colors_; }
  std::vector<glm::vec3> &normals() { return normals_; }

  std::vector<glm::vec2> &uvs() { return uvs_; }

  std::vector<unsigned int> &indices() { return indices_; }

 private:
  std::vector<glm::vec3> positions_;
  std::vector<glm::vec3> normals_;
  std::vector<glm::vec3> colors_;
  std::vector<glm::vec2> uvs_;
  std::vector<unsigned int> indices_;
};
} // namespace gl

#endif // OPENGL_SNIPPET_MODEL_H
