//
// Created by Neo on 19/10/2017.
//

#include "framebuffer.h"

namespace gl {
FrameBuffer::FrameBuffer(int internal_format, int width, int height,
                         bool anti_aliasing) {
  anti_aliasing_ = anti_aliasing;

  width_ = width;
  height_ = height;

  if (anti_aliasing_) {
    glGenFramebuffers(1, &fbo_intermediate_id_);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_intermediate_id_);

    GLuint multi_sampled_color_texture_;
    glGenTextures(1, &multi_sampled_color_texture_);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, multi_sampled_color_texture_);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, internal_format,
                            width_, height_, GL_TRUE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D_MULTISAMPLE,
                           multi_sampled_color_texture_, 0);

    GLuint multi_sampled_depth_texture_;
    glGenTextures(1, &multi_sampled_depth_texture_);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, multi_sampled_depth_texture_);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_DEPTH24_STENCIL8,
                            width_, height_, GL_TRUE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D_MULTISAMPLE,
                           multi_sampled_depth_texture_, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
      std::cout << "Unable to create intermediate frame buffer!\n";
      exit(-1);
    }
  }

  {
    fbo_id_ = 0;
    glGenFramebuffers(1, &fbo_id_);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_id_);

    color_texture_.Init(internal_format, width, height);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                         color_texture_.id(), 0);

    depth_texture_.Init(GL_DEPTH_COMPONENT, width, height);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                         depth_texture_.id(), 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
      std::cerr << "Unable to create frame buffer!\n";
      exit(-1);
    }
  }

  /// Decide type of the pixel buffer
  depth_capture_ =
      cv::Mat(depth_texture_.height(), depth_texture_.width(), CV_32F);
  switch (internal_format) {
    case GL_RGBA32F: // Remain channels (value passing)
      format_ = GL_RGBA;
      type_ = GL_FLOAT;
      color_capture_ =
          cv::Mat(color_texture_.height(), color_texture_.width(), CV_32FC4);
      break;
    case GL_RGBA: // RGB->BGR in opencv2
      format_ = GL_BGRA;
      type_ = GL_UNSIGNED_BYTE;
      color_capture_ =
          cv::Mat(color_texture_.height(), color_texture_.width(), CV_8UC4);
      break;
    case GL_RGB: // RGB->BGR in opencv2
    default:
      format_ = GL_BGR;
      type_ = GL_UNSIGNED_BYTE;
      color_capture_ =
          cv::Mat(color_texture_.height(), color_texture_.width(), CV_8UC3);
      break;
  }
}

void FrameBuffer::Blit() {
  if (!anti_aliasing_) {
    std::cerr << "NOT using anti-aliasing, abort blit" << std::endl;
    return;
  }
  glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_intermediate_id_);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_id_);
  glBlitFramebuffer(0, 0, width_, height_, 0, 0, width_, height_,
                    GL_COLOR_BUFFER_BIT, GL_NEAREST);
  glBlitFramebuffer(0, 0, width_, height_, 0, 0, width_, height_,
                    GL_DEPTH_BUFFER_BIT, GL_NEAREST);
}

void FrameBuffer::Bind() {
  glBindFramebuffer(GL_FRAMEBUFFER, anti_aliasing_ ? fbo_intermediate_id_ : fbo_id_);
  glViewport(0, 0, color_texture_.width(), color_texture_.height());
}

cv::Mat &FrameBuffer::CaptureColor() {
  color_texture_.Bind(0);
  glGetTexImage(GL_TEXTURE_2D, 0, format_, type_, color_capture_.data);
  cv::flip(color_capture_, color_capture_, 0);
  return color_capture_;
}

cv::Mat &FrameBuffer::CaptureDepth() {
  depth_texture_.Bind(0);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
                depth_capture_.data);
  cv::flip(depth_capture_, depth_capture_, 0);
  return depth_capture_;
}
} // namespace gl