//
// Created by Neo on 14/08/2017.
//

#include "window.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

namespace gl {
int Window::screenshot_index = 0;

Window::Window(std::string window_name, int width, int height,
               bool visible) {
  Init(window_name, width, height, visible);
}

void Window::Init(std::string window_name, int width, int height,
                  bool visible) {
  // Initialise GLFW
  if (!glfwInit()) {
    std::cerr << "Failed to initialize GLFW." << std::endl;
    exit(1);
  }
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_VISIBLE, visible);

  // anti-aliasing
  glfwWindowHint(GLFW_SAMPLES, 4);

  // Open a window and create its OpenGL context
  window_ = glfwCreateWindow(width, height, window_name.c_str(), NULL, NULL);
  if (window_ == NULL) {
    std::cerr << "Failed to open GLFW window." << std::endl;
    glfwTerminate();
    exit(1);
  }
  glfwMakeContextCurrent(window_);
  get_viewport();

  // Initialize GLEW
  glewExperimental = GL_TRUE; // Needed for core profile
  if (glewInit() != GLEW_OK) {
    std::cerr << "Failed to initialize GLEW." << std::endl;
    glfwTerminate();
    exit(1);
  }

  // Ensure we can capture the escape key being pressed below
  glfwSetKeyCallback(window_, OnKeyPressed);
}

void Window::Resize(int width, int height) {
  glfwSetWindowSize(window_, width, height);
  glfwPollEvents();
  glViewport(0, 0, width_, height_);
}

void Window::Bind() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, width_, height_);
}

void Window::get_viewport(){
  GLint dims[4] = {0};
  glGetIntegerv(GL_VIEWPORT, dims);
  width_ = dims[2];
  height_ = dims[3];
}

cv::Mat Window::CaptureRGB() {
  cv::Mat rgb = cv::Mat(height_, width_, CV_8UC3);
  glReadPixels(0, 0, width_, height_, GL_BGR, GL_UNSIGNED_BYTE, rgb.data);
  cv::flip(rgb, rgb, 0);
  return rgb;
}

cv::Mat Window::CaptureRGBA() {
  cv::Mat rgba = cv::Mat(height_, width_, CV_8UC4);
  glReadPixels(0, 0, width_, height_, GL_BGRA, GL_UNSIGNED_BYTE, rgba.data);
  cv::flip(rgba, rgba, 0);
  return rgba;
}

/**
 * Perform this BEFORE swap buffer
 * @return depth, to be processed by @class Camera, with z_near and z_far
 */
cv::Mat Window::CaptureDepth() {
  cv::Mat depth = cv::Mat(height_, width_, CV_32F);
  glReadBuffer(GL_BACK);
  glReadPixels(0, 0, width_, height_, GL_DEPTH_COMPONENT, GL_FLOAT, depth.data);
  cv::flip(depth, depth, 0);
  return depth;
}
} // namespace gl