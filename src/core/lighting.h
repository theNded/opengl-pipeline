//
// Created by wei on 17-12-11.
//

#ifndef OPENGL_WRAPPER_LIGHT_H
#define OPENGL_WRAPPER_LIGHT_H

#include <glm/glm.hpp>
#include <opencv2/opencv.hpp>
#include <vector>

namespace gl {
class Lighting {
public:
  /// Lights
  int light_count;
  std::string light_count_str;
  std::vector<glm::vec3> light_positions;
  std::vector<glm::vec3> light_directions;
  glm::vec3 light_color;
  float light_power;

  /// Materilas
  glm::vec3 material_ambient_coeff;
  glm::vec3 material_specular_coeff;

  void Load(std::string path) {
    cv::FileStorage fs(path, cv::FileStorage::READ);
    if (!fs.isOpened()) {
      std::cerr << "Unable to open file " << path << std::endl;
      exit(1);
    }

    // Load lighting
    /// For spot-lights
    cv::FileNode fs_positions = fs["light_positions"];
    for (auto iter = fs_positions.begin();
         iter != fs_positions.end();
         ++iter) {
      light_positions.push_back(glm::vec3(
          (float) (*iter)["x"],
          (float) (*iter)["y"],
          (float) (*iter)["z"]));
    }

    /// For directional-lights.
    cv::FileNode fs_directions = fs["light_directions"];
    for (auto iter = fs_directions.begin();
         iter != fs_directions.end();
         ++iter) {
      light_directions.push_back(glm::vec3(
          (float) (*iter)["x"],
          (float) (*iter)["y"],
          (float) (*iter)["z"]));
    }

    // They should be conflict
    assert(light_positions.size() * light_directions.size() == 0);
    assert(light_positions.size() + light_directions.size() > 0);

    light_count = light_positions.size();
    if (light_directions.size() > light_positions.size()) {
      light_count = light_directions.size();
    }

    std::stringstream ss;
    ss << light_count;
    light_count_str = ss.str();
    cv::FileNode fs_light_color = fs["light_color"];
    light_color = glm::vec3(
        (float) (fs_light_color)["r"],
        (float) (fs_light_color)["g"],
        (float) (fs_light_color)["b"]);
    light_power = (float) fs["light_power"];

    // Load material
    cv::FileNode fs_material_ambient_coeff = fs["material_ambient_coeff"];
    material_ambient_coeff = glm::vec3(
        (float) (fs_material_ambient_coeff)["r"],
        (float) (fs_material_ambient_coeff)["g"],
        (float) (fs_material_ambient_coeff)["b"]);

    cv::FileNode fs_material_specular_coeff = fs["material_specular_coeff"];
    material_specular_coeff = glm::vec3(
        (float) (fs_material_specular_coeff)["r"],
        (float) (fs_material_specular_coeff)["g"],
        (float) (fs_material_specular_coeff)["b"]);
  }
};
} // namespace gl

#endif // OPENGL_WRAPPER_LIGHT_H
