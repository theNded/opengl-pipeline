//
// Created by Neo on 14/08/2017.
//

#ifndef OPENGL_SNIPPET_WINDOW_H
#define OPENGL_SNIPPET_WINDOW_H

#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <opencv2/opencv.hpp>

// set window size
// get screen capture
namespace gl {
class Window {
public:
  Window() = default;

  /**
   * For macOS, @width and @height could cause 2x framebuffer size,
   * set it carefully
   * @param window_name
   * @param width
   * @param height
   * @param visible
   */
  Window(std::string window_name, int width, int height,
         bool visible = true);

  void Init(std::string window_name, int width, int height,
            bool visible = true);

  void Resize(int width, int height);

  /**
   * Bind the default framebuffer that come up with window
   */
  void Bind();

  /// GLFW operations
  void swap_buffer() {
    glfwSwapBuffers(window_);
    glfwPollEvents();
  }

  int should_close() { return glfwWindowShouldClose(window_); }

  int get_key(int key) { return glfwGetKey(window_, key); }

  /// Properties
  const GLint width() { return width_; }

  const GLint height() { return height_; }

  /// Screenshot utilities. A temporary solution.
  /// To take screenshots reliably, render to a fbo and use fbo.Capture().
  cv::Mat CaptureRGB();

  cv::Mat CaptureRGBA();

  cv::Mat CaptureDepth();

  // TODO: update it
  static int screenshot_index;

  static void OnKeyPressed(GLFWwindow *window, int key, int scancode,
                           int action, int mods) {
    if (action == GLFW_PRESS) {
      if (key == GLFW_KEY_ENTER) {
        screenshot_index++;
      }
    }
  }

private:
  GLFWwindow *window_;

  // Real size of the framebuffer
  void get_viewport();

  GLint width_;
  GLint height_;
};
} // namespace gl

#endif // OPENGL_SNIPPET_WINDOW_H
