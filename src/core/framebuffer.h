//
// Created by Neo on 19/10/2017.
//

#ifndef OPENGL_WRAPPER_FRAME_BUFFER_H
#define OPENGL_WRAPPER_FRAME_BUFFER_H

#include <GL/glew.h>
#include <opencv2/opencv.hpp>

#include "texture.h"

namespace gl {
/**
 * Framebuffer - intermediate place holding results
 * For easier access, directly render to texture with or without anti-aliasing
 */
class FrameBuffer {
public:
  FrameBuffer() = default;

  FrameBuffer(int internal_type, int width, int height,
              bool anti_aliasing = false);

  // Bind the selected framebuffer
  void Bind();

  // For anti-aliasing offscreen rendering
  void Blit();

  // pixel unit
  const GLint width() { return width_; }

  const GLint height() { return height_; }

  GLuint &id() { return fbo_id_; }

  Texture &color_texture() { return color_texture_; }

  Texture &depth_texture() { return depth_texture_; }

  cv::Mat &CaptureColor();

  cv::Mat &CaptureDepth();

private:
  GLenum format_;
  GLenum type_;
  int width_;
  int height_;
  bool anti_aliasing_ = false;

  // used for anti-aliasing
  GLuint fbo_intermediate_id_;
  GLuint fbo_id_;

  // Textures served as renderbuffers for easier visualization and transferring
  Texture color_texture_;
  Texture depth_texture_;

  cv::Mat color_capture_;
  cv::Mat depth_capture_;

};
} // namespace gl

#endif // OPENGL_WRAPPER_FRAME_BUFFER_H
