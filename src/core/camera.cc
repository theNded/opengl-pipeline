//
// Created by Neo on 14/08/2017.
//

#include "camera.h"
#include <glm/gtc/matrix_transform.hpp>

namespace gl {
Camera::Camera(int width, int height, float fov, float z_near, float z_far) {
  set_projection(width, height, fov, z_near, z_far);
  view_ = glm::mat4(1.0f);
  model_ = glm::mat4(1.0f);
}

cv::Matx33f Camera::projection_to_hartley(int width, int height) {
  cv::Matx33f K = cv::Matx33f::zeros();
  K(0, 0) = projection_[0][0] * width / 2;
  K(0, 2) = (projection_[2][0] + 1) * width / 2;
  K(1, 1) = -projection_[1][1] * height / 2;
  K(1, 2) = (1 - projection_[2][1]) * height / 2;
  K(2, 2) = 1;
  return K;
}

void Camera::set_projection(int width, int height,
                            float fov,
                            float z_near, float z_far) {
  width_ = width;
  height_ = height;
  fov_ = fov;
  z_near_ = z_near;
  z_far_ = z_far;
  projection_ =
      glm::perspective(fov_, (float) width_ / (float) height_, z_near_, z_far_);
}

void Camera::set_projection_from_hartley(const cv::Mat_<float> K, int width,
                                         int height, float z_near,
                                         float z_far) {
  set_projection_from_hartley(K[0][0], K[0][2], width,
                              K[1][1], K[1][2], height,
                              z_near, z_far);
}

void Camera::set_projection_from_hartley(float fx, float cx, int width,
                                         float fy, float cy, int height,
                                         float z_near, float z_far) {
  width_ = width;
  height_ = height;
  z_near_ = z_near;
  z_far_ = z_far;

  projection_ = glm::mat4(0);
  projection_[0][0] = 2 * fx / width_;
  projection_[2][0] = 2 * cx / width_ - 1;

  projection_[1][1] = -2 * fy / height_;
  projection_[2][1] = -2 * cy / height_ + 1;

  projection_[2][2] = (z_far_ + z_near_) / (z_far_ - z_near_);
  projection_[3][2] = -(2 * z_near_ * z_far_) / (z_far_ - z_near_);

  projection_[2][3] = 1;
}

cv::Matx34f Camera::view_to_hartley() {
  cv::Matx34f T;
  glm::mat4 view_t = glm::transpose(view_);

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 4; ++j) {
      T(i, j) = view_t[i][j];
    }
  }

  return T;
}

void Camera::set_view(const cv::Mat_<float> R, const cv::Mat_<float> t) {
  view_ = glm::mat4(1);
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      view_[i][j] = R[i][j];
    }
    view_[i][3] = t[i][0];
  }
  view_ = glm::transpose(view_);
}

void Camera::set_mvp_from_hartley(const cv::Mat_<float> P,
                                  int width, int height,
                                  float z_near, float z_far) {
  cv::Mat_<float> K, R, C;
  cv::decomposeProjectionMatrix(P, K, R, C);
  set_projection_from_hartley(K, width, height, z_near, z_far);
  C = C / C[3][0];
  cv::Mat_<float> C_normalized = C.rowRange(0, 3);
  set_view(R, -R * C_normalized); // t = -R * C
}

void Camera::SwitchInteraction(bool enable_interaction) {
  interaction_enabled_ = enable_interaction;
  // On initialization
  if (interaction_enabled_) {
    position_ = glm::vec3(0, 0, 0);
    azimuth_ = (float) M_PI;
    elevation_ = 0.0f;
  }
}

void Camera::UpdateView(Window &window) {
  if (!interaction_enabled_)
    return;

  static double last_time = glfwGetTime();
  double current_time = glfwGetTime();
  float delta_time = float(current_time - last_time);

  if (window.get_key(GLFW_KEY_I)) {
    elevation_ += kRotateSpeed * delta_time;
  } else if (window.get_key(GLFW_KEY_K)) {
    elevation_ -= kRotateSpeed * delta_time;
  } else if (window.get_key(GLFW_KEY_J)) {
    azimuth_ += kRotateSpeed * delta_time;
  } else if (window.get_key(GLFW_KEY_L)) {
    azimuth_ -= kRotateSpeed * delta_time;
  }

  // Compute new orientation
  glm::vec3 look_direction(cos(elevation_) * sin(azimuth_), sin(elevation_),
                           cos(elevation_) * cos(azimuth_));

  glm::vec3 move_direction(cos(elevation_) * sin(azimuth_), 0,
                           cos(elevation_) * cos(azimuth_));

  glm::vec3 right =
      glm::vec3(sin(azimuth_ - M_PI_2), 0, cos(azimuth_ - M_PI_2));

  glm::vec3 up = glm::cross(right, look_direction);

  if (window.get_key(GLFW_KEY_W) == GLFW_PRESS) {
    position_ += move_direction * kMoveSpeed * delta_time;
  }
  if (window.get_key(GLFW_KEY_S) == GLFW_PRESS) {
    position_ -= move_direction * kMoveSpeed * delta_time;
  }
  if (window.get_key(GLFW_KEY_D) == GLFW_PRESS) {
    position_ += right * kMoveSpeed * delta_time;
  }
  if (window.get_key(GLFW_KEY_A) == GLFW_PRESS) {
    position_ -= right * kMoveSpeed * delta_time;
  }
  if (window.get_key(GLFW_KEY_SPACE) == GLFW_PRESS) {
    position_.y += kMoveSpeed * delta_time;
  }
  if (window.get_key(GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
    position_.y -= kMoveSpeed * delta_time;
  }

  // Camera matrix
  view_ = glm::lookAt(position_, position_ + look_direction, up);

  last_time = current_time;
}

cv::Mat Camera::ConvertDepthBuffer(cv::Mat &depthf, float factor) {
  cv::Mat depths = cv::Mat(depthf.rows, depthf.cols, CV_16UC1);
  for (int i = 0; i < depthf.rows; ++i) {
    for (int j = 0; j < depthf.cols; ++j) {
      float z = depthf.at<float>(i, j);
      if (std::isnan(z) || std::isinf(z)) {
        depths.at<unsigned short>(i, j) = 0;
      } else {
        float clip_z = 2 * z - 1; // [0,1] -> [-1,1]
        // [-(n+f)/(n-f)] + [2nf/(n-f)] / w_z = clip_z
        GLfloat world_z = 2 * z_near_ * z_far_ /
                          (clip_z * (z_near_ - z_far_) + (z_near_ + z_far_));
        float d = world_z * factor;
        d = (d > factor * 5) ? factor * 5 : d;
        depths.at<unsigned short>(i, j) = (unsigned short) (d);
      }
    }
  }
  return depths;
}
} // namespace gl