//
// Created by Neo on 14/08/2017.
//

#ifndef OPENGL_SNIPPET_ARGS_H
#define OPENGL_SNIPPET_ARGS_H

/// VAO: arg1, arg2, ..., argn
/// VBO: buf1, buf2, ..., bufn
#include <GL/glew.h>
#include <iostream>
#include <vector>

#ifdef USE_CUDA_GL
#include <cuda_gl_interop.h>
#include <driver_types.h>
#endif

namespace gl {

/**
 * Encapsule input vertex arrays such as
 * layout(location = 0) in vec3 in_position;
 */
struct ArgAttrib {
  GLuint buffer; // ARRAY_BUFFER, ELEMENT_ARRAY_BUFFER
  GLint size;    // number of bytes per unit, sizeof(float),
  GLint count;   // number of units (1, 2, 3, ...)
  GLenum type;   // GL_FLOAT, ...

  ArgAttrib(GLuint _buffer, GLint _size, GLint _count, GLenum _type) {
    buffer = _buffer;
    size = _size;
    count = _count;
    type = _type;
  }

  // Add items here when needed
  ArgAttrib(GLuint _buffer, GLuint _input) {
    buffer = _buffer;
    switch (_input) {
      case GL_FLOAT_VEC3:
        size = sizeof(float);
        count = 3;
        type = GL_FLOAT;
        break;
      case GL_FLOAT_VEC2:
        size = sizeof(float);
        count = 2;
        type = GL_FLOAT;
        break;
      case GL_FLOAT:
        size = sizeof(float);
        count = 1;
        type = GL_FLOAT;
        break;
      case GL_UNSIGNED_INT:
        size = sizeof(unsigned int);
        count = 1;
        type = GL_UNSIGNED_INT;
        break;
      case GL_UNSIGNED_BYTE:
        size = sizeof(unsigned char);
        count = 1;
        type = GL_UNSIGNED_BYTE;
        break;
      default:
        std::cerr << "ArgAttrib undefined, please add it!" << std::endl;
        exit(1);
    }
  }
};

class Args {
public:
  Args();

  explicit Args(int argn, bool use_cuda = false);

  void Init(int argn, bool use_cuda = false);

  ~Args();

  const GLuint &vao() const { return vao_; }

  // i-th buffer, arg attributes, buffer-size
  void InitBuffer(GLuint i, ArgAttrib arg_attrib, size_t max_size);

  void BindBuffer(GLuint i, ArgAttrib arg_attrib, size_t size, void *data);

private:
  int argn_;
  GLuint vao_;
  std::vector<GLuint> vbos_;

  bool use_cuda_;
#ifdef USE_CUDA_GL
  std::vector<cudaGraphicsResource_t> cuda_res_;
#endif
};
} // namespace gl

#endif // OPENGL_SNIPPET_ARGS_H
