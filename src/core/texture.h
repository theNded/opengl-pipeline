//
// Created by Neo on 14/08/2017.
//

#ifndef OPENGL_SNIPPET_TEXTURE_H
#define OPENGL_SNIPPET_TEXTURE_H

#include <GL/glew.h>
#include <opencv2/opencv.hpp>
#include <string>

namespace gl {
class Texture {
public:
  /// Texture t = Texture(); t.Init( /* for read or write */);
  Texture() = default;

  ~Texture();

  // Load (but not init, usually performed before OpenGL window initialization)
  void Load(std::string texture_path);

  void Load(cv::Mat &texture);

  /// Init for reading. Load from file.
  void Init(std::string texture_path, bool use_mipmap = false);

  void Init(cv::Mat &texture, bool use_mipmap = false);

  // Init for writing.
  void Init(GLint internal_format, int width, int height);

  void Bind(int texture_idx);

  cv::Mat &image() { return texture_; }

  const GLuint &id() const { return texture_id_; }

  // pixel unit
  const int width() const { return width_; }

  const int height() const { return height_; }

private:
  cv::Mat texture_;
  int width_;
  int height_;
  GLuint texture_id_;

  bool texture_generated_ = false;

  void ConfigTexture(bool use_mipmap);
};
} // namespace gl

#endif // OPENGL_SNIPPET_TEXTURE_H
