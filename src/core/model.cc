//
// Created by Neo on 22/08/2017.
//

#include "model.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <unordered_map>

namespace gl {

Model::Model(std::string path) { LoadObj(path, true); }

/// Not a robust version !
// TODO: robustify OBJ loading; add .ply loading
void Model::LoadObj(std::string path, bool with_uv) {
  std::cout << "Loading " << path << " ..." << std::endl;

  // Assuming it is .obj
  std::ifstream obj_stream(path, std::ios::in);
  if (!obj_stream.is_open()) {
    std::cerr << "Fail to open " << path << std::endl;
    exit(1);
  }

  std::vector<glm::vec3> raw_positions;
  std::vector<glm::vec2> raw_uvs;
  std::vector<glm::vec3> raw_normals;
  std::vector<std::string> raw_indices;

  if (with_uv) {
    for (std::string line; std::getline(obj_stream, line);) {
      std::stringstream line_stream(line);
      std::string tag;
      line_stream >> tag;
      if (tag == "v") { // vertex coordinate
        glm::vec3 vertex;
        line_stream >> vertex.x >> vertex.y >> vertex.z;
        raw_positions.push_back(vertex);
      } else if (tag == "vt") { // uv coordinate
        glm::vec2 uv;
        line_stream >> uv.x >> uv.y;
        raw_uvs.push_back(uv);
      } else if (tag == "vn") { // normal
        glm::vec3 normal;
        line_stream >> normal.x >> normal.y >> normal.z;
        raw_normals.push_back(normal);
      } else if (tag == "f") { // indices
        std::string slashed_index;
        for (int i = 0; i < 3; ++i) {
          line_stream >> slashed_index;
          raw_indices.push_back(slashed_index);
        }
      }
    }

    // Assign new indices for "vi/uvi/ni" -> i
    std::unordered_map<std::string, unsigned int> new_indices;
    unsigned int new_index = 0;
    for (auto i = raw_indices.begin(); i != raw_indices.end(); ++i) {
      if (new_indices.find(*i) == new_indices.end()) {
        new_indices[*i] = new_index;
        new_index++;
      }
    }

    // Generate new vectors
    for (auto i = raw_indices.begin(); i != raw_indices.end(); ++i) {
      indices_.push_back(new_indices[*i]);

      // Existing index
      if (new_indices[*i] < positions_.size())
        continue;

      std::stringstream indice_group_stream(*i);
      char slash;
      unsigned int vertex_index, uv_index, normal_index;
      indice_group_stream >> vertex_index >> slash >> uv_index >> slash >>
          normal_index;

      positions_.push_back(raw_positions[vertex_index - 1]);
      uvs_.push_back(raw_uvs[uv_index - 1]);
      normals_.push_back(raw_normals[normal_index - 1]);
    }
  }

  else {
    for (std::string line; std::getline(obj_stream, line);) {
      std::stringstream line_stream(line);
      std::string tag;
      line_stream >> tag;
      if (tag == "v") { // vertex coordinate
        glm::vec3 vertex;
        line_stream >> vertex.x >> vertex.y >> vertex.z;
        raw_positions.push_back(vertex);
      } else if (tag == "vn") { // normal
        glm::vec3 normal;
        line_stream >> normal.x >> normal.y >> normal.z;
        raw_normals.push_back(normal);
      } else if (tag == "f") { // indices
        std::string slashed_index;
        for (int i = 0; i < 3; ++i) {
          line_stream >> slashed_index;
          raw_indices.push_back(slashed_index);
        }
      }
    }

    // Assign new indices for "vi//ni" -> i
    std::unordered_map<std::string, unsigned int> new_indices;
    unsigned int new_index = 0;
    for (auto i = raw_indices.begin(); i != raw_indices.end(); ++i) {
      if (new_indices.find(*i) == new_indices.end()) {
        new_indices[*i] = new_index;
        new_index++;
      }
    }

    // Generate new vectors
    for (auto i = raw_indices.begin(); i != raw_indices.end(); ++i) {
      indices_.push_back(new_indices[*i]);

      // Existing index
      if (new_indices[*i] < positions_.size())
        continue;

      std::stringstream indice_group_stream(*i);
      char slash;
      unsigned int vertex_index, normal_index;
      indice_group_stream >> vertex_index >> slash >> slash >> normal_index;

      positions_.push_back(raw_positions[vertex_index - 1]);
      normals_.push_back(raw_normals[normal_index - 1]);
    }
  }

  std::cout << "Loading finished ..." << std::endl;
}

void Model::SaveObj(std::string path, bool with_uv) {
  std::ofstream out(path);
  std::stringstream ss;

  std::cout << "Writing " << positions_.size() << " vertices" << std::endl;
  for (uint i = 0; i < positions_.size(); ++i) {
    ss.str("");
    ss << "v ";
    ss << positions_[i].x << " " << positions_[i].y << " " << positions_[i].z
       << "\n";
    out << ss.str();
  }

  std::cout << "Writing " << normals_.size() << " normals" << std::endl;
  for (uint i = 0; i < normals_.size(); ++i) {
    ss.str("");
    ss << "vn ";
    ss << normals_[i].x << " " << normals_[i].y << " " << normals_[i].z << "\n";
    out << ss.str();
  }

  if (with_uv) {
    std::cout << "Writing " << uvs_.size() << " uvs" << std::endl;
    for (uint i = 0; i < uvs_.size(); ++i) {
      ss.str("");
      ss << "vt ";
      ss << uvs_[i].x << " " << uvs_[i].y << "\n";
      out << ss.str();
    }
  }

  std::cout << "Writing faces" << std::endl;
  for (uint i = 0; i < indices_.size(); i += 3) {
    ss.str("");
    int fx = indices_[i + 0];
    int fy = indices_[i + 1];
    int fz = indices_[i + 2];

    if (with_uv) {
      ss << "f ";
      ss << fx << "/" << fx << "/" << fx << " ";
      ss << fy << "/" << fy << "/" << fy << " ";
      ss << fz << "/" << fz << "/" << fz << "\n";      
    } else {
      ss << "f ";
      ss << fx << "//" << fx << " ";
      ss << fy << "//" << fy << " ";
      ss << fz << "//" << fz << "\n";
    }
    out << ss.str();
  }
  std::cout << "Finished writing ...\n";
}

void Model::LoadPly(std::string path) {
  // Assuming it is .ply
  // -- This is a temporary solution and only suitable for specific ply files.
  std::ifstream ply_stream(path, std::ios::in);
  if (!ply_stream.is_open()) {
    std::cerr << "Fail to open " << path << std::endl;
    return;
  }

  // Read header
  std::string padding;
  int vtx_count, face_count;
  // ply; format; comment
  for (int i = 0; i < 3; ++i) {
    std::getline(ply_stream, padding);
  }
  ply_stream >> padding >> padding >> vtx_count;
  for (int i = 0; i < 11; ++i) {
    std::getline(ply_stream, padding);
  }
  ply_stream >> padding >> padding >> face_count;
  std::cout << vtx_count << " " << face_count << std::endl;

  for (int i = 0; i < 3; ++i) {
    std::getline(ply_stream, padding);
  }

  float x, y, z, nx, ny, nz, r, g, b, a;
  for (int i = 0; i < vtx_count; ++i) {
    ply_stream >> x >> y >> z >> nx >> ny >> nz >> r >> g >> b >> a;
    positions_.emplace_back(glm::vec3(x, y, z));
    colors_.emplace_back(glm::vec3(r, g, b) / 255.0f);
    normals_.emplace_back(glm::vec3(nx, ny, nz));
  }

  unsigned int f, fx, fy, fz;
  for (int i = 0; i < face_count; ++i) {
    ply_stream >> f >> fx >> fy >> fz;
    indices_.emplace_back(fx);
    indices_.emplace_back(fy);
    indices_.emplace_back(fz);
  }

}
} // namespace gl