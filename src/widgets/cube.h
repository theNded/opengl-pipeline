//
// Created by wei on 17-12-11.
//

#ifndef OPENGL_WRAPPER_CUBE_H
#define OPENGL_WRAPPER_CUBE_H

static float kPositions[] = {1.0f,  -1.0f, 0.0f,  1.0f,  1.0f,  0.0f,
                             -1.0f, 1.0f,  0.0f,  -1.0f, -1.0f, 0.0f,
                             1.0f,  -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f,
                             -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f};

static float kColors[] = {1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
                          0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

static unsigned char kIndices[] = {0, 1, 2, 2, 3, 0, 4, 6, 5, 4, 7, 6,
                                   2, 7, 3, 7, 6, 2, 0, 4, 1, 4, 1, 5,
                                   6, 2, 1, 1, 6, 5, 0, 3, 7, 0, 7, 4};

#endif // OPENGL_WRAPPER_CUBE_H
