//
// Created by wei on 17-12-18.
//

#ifndef OPENGL_WRAPPER_RECT_H
#define OPENGL_WRAPPER_RECT_H

static const GLfloat kVertices[] = {
    -1.0f, -1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    1.0f,  1.0f, 0.0f,
};

#endif //OPENGL_WRAPPER_RECT_H
